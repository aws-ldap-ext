all:
	@mkdir -p obj
	@gnatmake -Pldapext

clean:
	@rm -rf obj/*

distclean:
	@rm -rf obj
