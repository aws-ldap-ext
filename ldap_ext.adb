with AWS.LDAP;
with AWS.LDAP.Client;
with Ada.Strings.Unbounded;
with AWS.LDAP.Thin;
with Interfaces.C.Strings;

procedure LDAP_Ext is

   use Ada;
   use Ada.Strings.Unbounded;
   use AWS;

   Directory : LDAP.Client.Directory;
   Host      : String := "172.24.0.8";
   Base_DN   : String := "dc=swiss-it,dc=ch";
   Filter    : String := "(objectclass=*)";

   pragma Linker_Options ("-laws");
begin

   Directory := LDAP.Client.Init (Host => Host);

   LDAP.Client.Bind (Dir      => Directory,
                     Login    => "cn=admin,dc=swiss-it,dc=ch",
                     Password => "asdfgh");

   declare
      use LDAP.Client.LDAP_Mods;

      DN    : String := "ou=test1,ou=aws,dc=swiss-it,dc=ch";
      Mods1 : Vector;
      Mods2 : Vector;
      Mod1  : LDAP.Client.Mod_Element (2);
      Mod2  : LDAP.Client.Mod_Element (1);
      Mod3  : LDAP.Client.Mod_Element (1);
      Mod4  : LDAP.Client.Mod_Element (1);
   begin
      --  Add an entry.
      Mod1.Mod_Op         := LDAP.Client.LDAP_Mod_Add;
      Mod1.Mod_Type       := To_Unbounded_String ("objectClass");
      Mod1.Mod_Values (1) := To_Unbounded_String ("organizationalUnit");
      Mod1.Mod_Values (2) := To_Unbounded_String ("top");

      Mod2.Mod_Op         := LDAP.Client.LDAP_Mod_Add;
      Mod2.Mod_Type       := To_Unbounded_String ("ou");
      Mod2.Mod_Values (1) := To_Unbounded_String ("test1");

      Mods1.Append (Mod1);
      Mods1.Append (Mod2);

      LDAP.Client.Add (Dir  => Directory,
                       DN   => DN,
                       Mods => Mods1);

      --  Modify added entry.
      Mod3.Mod_Op         := LDAP.Client.LDAP_Mod_Add;
      Mod3.Mod_Type       := To_Unbounded_String ("street");
      Mod3.Mod_Values (1) := To_Unbounded_String ("teststreet");

      Mod4.Mod_Op         := LDAP.Client.LDAP_Mod_Replace;
      Mod4.Mod_Type       := To_Unbounded_String ("street");
      Mod4.Mod_Values (1) := To_Unbounded_String ("teststreet-new");

      Mods2.Append (Mod3);
      Mods2.Append (Mod4);

      LDAP.Client.Modify (Dir  => Directory,
                          DN   => DN,
                          Mods => Mods2);

      --  Delete entry again.
      LDAP.Client.Delete (Dir => Directory,
                          DN  => DN);
   end;
   LDAP.Client.Unbind (Directory);

end LDAP_Ext;

--  procedure LDAP_Ext is
--
--     use Ada;
--     use Ada.Strings.Unbounded;
--     use AWS;
--     use LDAP.Client.LDAP_Mods;
--
--
--     Mods1 : Vector;
--     Mods2 : Vector;
--     Mod1  : LDAP.Client.Mod_Element;
--
--     pragma Linker_Options ("-laws");
--  begin
--     Mod1.Mod_Op         := LDAP.Client.LDAP_Mod_Add;
--     Mod1.Mod_Type       := To_Unbounded_String ("test1");
--     Mod1.Mod_Values (1) := To_Unbounded_String ("test2");
--     Mod1.Mod_Values (2) := To_Unbounded_String ("test3");
--     Mod1.Mod_Values (3) := To_Unbounded_String ("test4");
--     Mod1.Mod_Values (4) := To_Unbounded_String ("test5");
--
--     Mods1.Append (Mod1);
--
--     declare
--        C_Mods : LDAP.Thin.LDAPMods :=
--          LDAP.Client.To_C (Mods => Mods1);
--     begin
--        LDAP.Client.Free (C_Mods (C_Mods'First)'Unchecked_Access);
--     end;
--  end LDAP_Ext;
